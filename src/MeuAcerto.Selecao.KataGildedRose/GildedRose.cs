﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    //constantes para facilitar alteracao caso necessario
    static class Constantes
    {
        public const double MINIMO_QUALIDADE = 0;
        public const int MAXIMO_QUALIDADE = 50;
    }

    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        //resolvi criar cada método separado para os itens porque fica mais fácil de alterar depois e se for incluir novas regras de negócio, basta criar o método necessário ou aproveitar um já feito
        private Item tratamentoQueijoBrie(Item item)
        {
            if (item.Qualidade < Constantes.MAXIMO_QUALIDADE)
            {
                if (item.PrazoParaVenda > 0)
                {
                    item.Qualidade += 1;
                }
                else
                {
                    item.Qualidade += 2;
                }
            }
   
            return item;

        }

        private Item tratamentoOutros(Item item)
        {
            if (item.Qualidade > Constantes.MINIMO_QUALIDADE)
            {
                if (item.PrazoParaVenda > 0)
                {
                    item.Qualidade -= 1;
                }
                else
                {
                    item.Qualidade -= 2;
                }
            }

            return item;
        }

        private Item tratamentoIngresso(Item item)
        {
            if (item.Qualidade < Constantes.MAXIMO_QUALIDADE) { 
                if (item.PrazoParaVenda < 11)
                {
                    item.Qualidade += 1;
                }
                else if (item.PrazoParaVenda < 6)
                {
                    item.Qualidade += 1;
                }
                else
                {
                    item.Qualidade += 1;
                }
            }

            if (item.PrazoParaVenda <= 0)
            {
                item.Qualidade = 0;
            }

            return item;
        }

        private Item tratamentoConjurados(Item item)
        {
            if (item.Qualidade > Constantes.MINIMO_QUALIDADE) item.Qualidade -= 2;

            return item;
        }

        public void AtualizarQualidade()
        {
            for (var i = 0; i < Itens.Count; i++)
            {
                //condicoes padroes para todos os itens nao precisam ser tratados varias vezes por causa da redundacia
                //insercao de um switch para ficar mais legivel o codigo 
                switch (Itens[i].Nome)
                {
                    case "Corselete +5 DEX":
                        Itens[i] = tratamentoOutros(Itens[i]);
                        break;
                    case "Queijo Brie Envelhecido":
                        Itens[i] = tratamentoQueijoBrie(Itens[i]);
                        break;
                    case "Elixir do Mangusto":
                        Itens[i] = tratamentoOutros(Itens[i]);
                        break;
                    case "Dente do Tarrasque":
                        //como dente do tarrasque não sofre perda de qualidade e nem de prazo, coloquei um continue pra seguir o for
                        continue;
                    case "Ingressos para o concerto do Turisas":
                        Itens[i] = tratamentoIngresso(Itens[i]);
                        break;
                    case "Bolo de Mana Conjurado":
                        Itens[i] = tratamentoConjurados(Itens[i]);
                        break;
                }

                //Como é uma ação comum para todos os itens, podemos deixar no final do tratamento deles o decréscimo no prazo
                Itens[i].PrazoParaVenda -= 1;
            }
        }
    }
}